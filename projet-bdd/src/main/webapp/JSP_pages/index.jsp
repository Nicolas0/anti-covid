<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Accueil</title>
        <link href="./style.css" rel="stylesheet">        
        <link href="./sidebar.css" rel="stylesheet">        
        <!-- Scrollbar Custom CSS -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">	
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    </head>
    <body>
	   	<div class="wrapper">
<%--       		<%@include file='./sidebar.jsp'%>
 --%>	   	
	   		<div id="content">
	   		
	   		    <nav class="navbar navbar-expand-lg navbar-light bg-light">
			        <div class="container-fluid">
			
			            <button type="button" id="sidebarCollapse" class="btn btn-info">
			                <i class="fas fa-align-left"></i>
			                <span>Toggle Sidebar</span>
			            </button>
			
			        </div>
    			</nav>
	   		
				<h1>Bienvenue</h1>
		       	
		       	<div class="alert alert-primary" role="alert">
		  			A simple primary alert check it out!
				</div>
	
				<div>
				 <a href="./JSP_pages/forms.jsp">Se connecter</a>
		      	</div> 
		      	
		      	<div>
		      	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin feugiat, quam in faucibus bibendum, libero ante condimentum tellus, faucibus suscipit nisl est et arcu. Cras finibus, quam nec convallis tincidunt, velit mauris faucibus orci, vel porta justo ipsum nec nibh. Maecenas nibh dui, porttitor in rhoncus in, mollis eu metus. Nunc quis dolor volutpat, interdum nibh at, placerat tellus. Aliquam ex lectus, dictum interdum sapien ut, sagittis interdum leo. Etiam at imperdiet metus, id fermentum metus. Vivamus condimentum consectetur velit pharetra molestie. Curabitur accumsan aliquet eros, id semper erat venenatis sit amet. Donec at ullamcorper mauris. Donec ac viverra neque. Cras finibus maximus ipsum, sed auctor dui scelerisque vitae. Etiam vel augue nisi.

Proin placerat semper risus vel finibus. Vivamus ut nibh sit amet est dictum interdum. Phasellus ac ex arcu. Aliquam dictum tortor eu aliquet maximus. Morbi nec massa consequat ligula pretium maximus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce aliquam ipsum ac ligula euismod ornare. Cras nulla quam, pulvinar eu sapien ut, rhoncus pretium dui. Praesent a ante arcu. Ut sit amet eleifend nunc. Donec imperdiet varius dignissim. Mauris semper blandit sollicitudin. Duis non dolor libero.

Aenean tristique, elit eu pellentesque tristique, mauris dolor aliquam ante, vel sagittis diam justo et lectus. Donec vitae massa tempus dui viverra rutrum non a erat. Nullam accumsan fringilla placerat. Maecenas posuere odio non tellus bibendum, id pellentesque leo varius. Integer interdum leo eget commodo interdum. Mauris dignissim rutrum ipsum, vel tempor risus. Pellentesque magna felis, varius sit amet faucibus eget, vulputate sed nulla.

Cras placerat ornare ligula, id volutpat nisl congue ac. Quisque nec tortor eget mauris fermentum lacinia in quis velit. Pellentesque felis leo, vestibulum vel vestibulum sit amet, condimentum vel nibh. In hac habitasse platea dictumst. Quisque facilisis, felis in sagittis fringilla, est augue tincidunt arcu, vitae imperdiet erat neque eget ipsum. Integer ut gravida diam. Nullam vitae commodo ex. Cras id efficitur dolor, quis hendrerit lorem. Sed in lorem sed odio posuere laoreet vitae vel eros. Nullam tincidunt nisl et nisl gravida, at gravida eros vestibulum. Phasellus purus tortor, ultrices ac auctor vitae, scelerisque ut orci. Nam vitae molestie ipsum.

Phasellus vel accumsan tellus, in semper orci. Vivamus vitae leo pharetra, varius ante eget, efficitur sapien. In eget felis sit amet ante aliquet dictum sed nec justo. Donec dui ex, sagittis vitae risus vitae, fermentum venenatis lorem. In vel gravida nulla, a imperdiet lorem. Duis aliquet ligula eu ipsum sodales, ac elementum nibh pellentesque. Suspendisse molestie sed lacus sed dignissim. Maecenas ac dui eu elit rhoncus malesuada ac egestas magna. Fusce dui velit, vehicula id orci euismod, rhoncus sollicitudin neque. Sed porttitor, urna ut scelerisque facilisis, ex ipsum aliquet dolor, eget pellentesque tellus lorem et elit. Proin felis libero, suscipit vitae sapien cursus, tincidunt imperdiet purus. Integer efficitur erat arcu. Sed rhoncus dolor vitae diam euismod tempus. Curabitur vehicula interdum libero.
      	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin feugiat, quam in faucibus bibendum, libero ante condimentum tellus, faucibus suscipit nisl est et arcu. Cras finibus, quam nec convallis tincidunt, velit mauris faucibus orci, vel porta justo ipsum nec nibh. Maecenas nibh dui, porttitor in rhoncus in, mollis eu metus. Nunc quis dolor volutpat, interdum nibh at, placerat tellus. Aliquam ex lectus, dictum interdum sapien ut, sagittis interdum leo. Etiam at imperdiet metus, id fermentum metus. Vivamus condimentum consectetur velit pharetra molestie. Curabitur accumsan aliquet eros, id semper erat venenatis sit amet. Donec at ullamcorper mauris. Donec ac viverra neque. Cras finibus maximus ipsum, sed auctor dui scelerisque vitae. Etiam vel augue nisi.

Proin placerat semper risus vel finibus. Vivamus ut nibh sit amet est dictum interdum. Phasellus ac ex arcu. Aliquam dictum tortor eu aliquet maximus. Morbi nec massa consequat ligula pretium maximus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce aliquam ipsum ac ligula euismod ornare. Cras nulla quam, pulvinar eu sapien ut, rhoncus pretium dui. Praesent a ante arcu. Ut sit amet eleifend nunc. Donec imperdiet varius dignissim. Mauris semper blandit sollicitudin. Duis non dolor libero.

Aenean tristique, elit eu pellentesque tristique, mauris dolor aliquam ante, vel sagittis diam justo et lectus. Donec vitae massa tempus dui viverra rutrum non a erat. Nullam accumsan fringilla placerat. Maecenas posuere odio non tellus bibendum, id pellentesque leo varius. Integer interdum leo eget commodo interdum. Mauris dignissim rutrum ipsum, vel tempor risus. Pellentesque magna felis, varius sit amet faucibus eget, vulputate sed nulla.

Cras placerat ornare ligula, id volutpat nisl congue ac. Quisque nec tortor eget mauris fermentum lacinia in quis velit. Pellentesque felis leo, vestibulum vel vestibulum sit amet, condimentum vel nibh. In hac habitasse platea dictumst. Quisque facilisis, felis in sagittis fringilla, est augue tincidunt arcu, vitae imperdiet erat neque eget ipsum. Integer ut gravida diam. Nullam vitae commodo ex. Cras id efficitur dolor, quis hendrerit lorem. Sed in lorem sed odio posuere laoreet vitae vel eros. Nullam tincidunt nisl et nisl gravida, at gravida eros vestibulum. Phasellus purus tortor, ultrices ac auctor vitae, scelerisque ut orci. Nam vitae molestie ipsum.

Phasellus vel accumsan tellus, in semper orci. Vivamus vitae leo pharetra, varius ante eget, efficitur sapien. In eget felis sit amet ante aliquet dictum sed nec justo. Donec dui ex, sagittis vitae risus vitae, fermentum venenatis lorem. In vel gravida nulla, a imperdiet lorem. Duis aliquet ligula eu ipsum sodales, ac elementum nibh pellentesque. Suspendisse molestie sed lacus sed dignissim. Maecenas ac dui eu elit rhoncus malesuada ac egestas magna. Fusce dui velit, vehicula id orci euismod, rhoncus sollicitudin neque. Sed porttitor, urna ut scelerisque facilisis, ex ipsum aliquet dolor, eget pellentesque tellus lorem et elit. Proin felis libero, suscipit vitae sapien cursus, tincidunt imperdiet purus. Integer efficitur erat arcu. Sed rhoncus dolor vitae diam euismod tempus. Curabitur vehicula interdum libero.

		      	      	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin feugiat, quam in faucibus bibendum, libero ante condimentum tellus, faucibus suscipit nisl est et arcu. Cras finibus, quam nec convallis tincidunt, velit mauris faucibus orci, vel porta justo ipsum nec nibh. Maecenas nibh dui, porttitor in rhoncus in, mollis eu metus. Nunc quis dolor volutpat, interdum nibh at, placerat tellus. Aliquam ex lectus, dictum interdum sapien ut, sagittis interdum leo. Etiam at imperdiet metus, id fermentum metus. Vivamus condimentum consectetur velit pharetra molestie. Curabitur accumsan aliquet eros, id semper erat venenatis sit amet. Donec at ullamcorper mauris. Donec ac viverra neque. Cras finibus maximus ipsum, sed auctor dui scelerisque vitae. Etiam vel augue nisi.

Proin placerat semper risus vel finibus. Vivamus ut nibh sit amet est dictum interdum. Phasellus ac ex arcu. Aliquam dictum tortor eu aliquet maximus. Morbi nec massa consequat ligula pretium maximus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce aliquam ipsum ac ligula euismod ornare. Cras nulla quam, pulvinar eu sapien ut, rhoncus pretium dui. Praesent a ante arcu. Ut sit amet eleifend nunc. Donec imperdiet varius dignissim. Mauris semper blandit sollicitudin. Duis non dolor libero.

Aenean tristique, elit eu pellentesque tristique, mauris dolor aliquam ante, vel sagittis diam justo et lectus. Donec vitae massa tempus dui viverra rutrum non a erat. Nullam accumsan fringilla placerat. Maecenas posuere odio non tellus bibendum, id pellentesque leo varius. Integer interdum leo eget commodo interdum. Mauris dignissim rutrum ipsum, vel tempor risus. Pellentesque magna felis, varius sit amet faucibus eget, vulputate sed nulla.

Cras placerat ornare ligula, id volutpat nisl congue ac. Quisque nec tortor eget mauris fermentum lacinia in quis velit. Pellentesque felis leo, vestibulum vel vestibulum sit amet, condimentum vel nibh. In hac habitasse platea dictumst. Quisque facilisis, felis in sagittis fringilla, est augue tincidunt arcu, vitae imperdiet erat neque eget ipsum. Integer ut gravida diam. Nullam vitae commodo ex. Cras id efficitur dolor, quis hendrerit lorem. Sed in lorem sed odio posuere laoreet vitae vel eros. Nullam tincidunt nisl et nisl gravida, at gravida eros vestibulum. Phasellus purus tortor, ultrices ac auctor vitae, scelerisque ut orci. Nam vitae molestie ipsum.

Phasellus vel accumsan tellus, in semper orci. Vivamus vitae leo pharetra, varius ante eget, efficitur sapien. In eget felis sit amet ante aliquet dictum sed nec justo. Donec dui ex, sagittis vitae risus vitae, fermentum venenatis lorem. In vel gravida nulla, a imperdiet lorem. Duis aliquet ligula eu ipsum sodales, ac elementum nibh pellentesque. Suspendisse molestie sed lacus sed dignissim. Maecenas ac dui eu elit rhoncus malesuada ac egestas magna. Fusce dui velit, vehicula id orci euismod, rhoncus sollicitudin neque. Sed porttitor, urna ut scelerisque facilisis, ex ipsum aliquet dolor, eget pellentesque tellus lorem et elit. Proin felis libero, suscipit vitae sapien cursus, tincidunt imperdiet purus. Integer efficitur erat arcu. Sed rhoncus dolor vitae diam euismod tempus. Curabitur vehicula interdum libero.

		      	
		      	
		      	</div>
			</div>	    
	   	</div>

      	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
   	    <!-- jQuery Custom Scroller CDN -->
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
      	
      	<script type="text/javascript">
      	$(document).ready(function () {

      	    $("#sidebar").mCustomScrollbar({
      	         theme: "minimal"
      	    });

      	    $('#sidebarCollapse').on('click', function () {
      	        // open or close navbar
      	        $('#sidebar').toggleClass('active');
      	        // close dropdowns
      	        $('.collapse.in').toggleClass('in');
      	        // and also adjust aria-expanded attributes we use for the open/closed arrows
      	        // in our CSS
      	        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
      	    });

      	});
		</script> 
      </body>
</html>