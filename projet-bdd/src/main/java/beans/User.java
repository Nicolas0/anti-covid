package beans;

public class User  {
	
	private int id;
	private String nom;
	private String prenom;
	private String rang;
	private String hash;
	private String login;
	
	public int getId() {
		return this.id;
	}
	
	public String getNom() {
		return this.nom;
	}

	public String getPrenom() {
		return this.prenom;
	}
	
	public String getHash() {
		return this.hash;
	}
	
	public String getLogin() {
		return this.login;
	}
	
	public String getRang() {
		return this.rang;
	}
	
	public void setId( int id ) {
		this.id = id;
	}
	
	public void setNom( String nom ) {
		this.nom = nom;
	}

	public void setPrenom( String prenom ) {
		this.prenom = prenom;
	}
	
	public void setHash( String hash ) {
		this.hash = hash;
	}
	
	public void setLogin( String login ) {
		this.login = login;
	}
	
	public void setRang( String rang ) {
		this.rang = rang;
	}
	
	public String toString() {
		return this.id + ": " + this.login + " " + this.prenom + " " + this.nom + 
				"\n rang: " + this.rang + "\n mot de passe: " + this.hash;
	}
}
