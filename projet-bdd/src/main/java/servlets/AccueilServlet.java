package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import sql.ConnectionBDD;
import sql.ConnectionBDD.*;

/**
 * Servlet implementation class LoginServlet
 */
//@WebServlet("/index")

public class AccueilServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccueilServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		this.getServletContext().getRequestDispatcher("/JSP_pages/bean.jsp").forward(request, response);
	
		response.setContentType("text/html");	
		
		HttpSession session = request.getSession();
		
		User current_user = (User) session.getAttribute("current_user");
		
		if(current_user == null) {
			
			request.getRequestDispatcher( "/JSP_pages/bean.jsp" ).forward( request, response );
		}
		else{
			if(current_user.getRang().trim().equals("basic_user")) {
				request.getRequestDispatcher( "/JSP_pages/logged.jsp" ).forward( request, response );
			}
			else {
				if(current_user.getRang().trim().equals("admin")) {
					request.getRequestDispatcher( "/JSP_pages/admin.jsp" ).forward( request, response );
				}
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("POST Connexion:");
		ConnectionBDD sc = new ConnectionBDD();
		sc.connect();
		
		String login = request.getParameter("login");
		String pwd = request.getParameter("password");
		String[] tabParam= new String[] {login, pwd};
		
		System.out.println(sc.getUser(login, pwd).toString());

		response.sendRedirect("/projet-bdd");

	}
}

