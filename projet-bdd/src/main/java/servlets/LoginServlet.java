package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import sql.ConnectionBDD;
import sql.ConnectionBDD.*;

/**
 * Servlet implementation class LoginServlet
 */
//@WebServlet("/login")

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/JSP_pages/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String login = request.getParameter("login");
		String pwd = request.getParameter("password");
		
		HttpSession session = request.getSession();
		ConnectionBDD sc = new ConnectionBDD();
		
		if((login != "") && (login != null) && (pwd != "") && (pwd != null) ) {
			
			User current_user = sc.getUser(login,pwd);
			if (current_user != null) {				
				System.out.println(current_user.toString());
				session.setAttribute("current_user",current_user);
				request.setAttribute("current_user",current_user);
			}
		}
		else {
			session.setAttribute("current_user",null);
			request.setAttribute("current_user",null);
		}
		
		response.sendRedirect("/projet-bdd/");
	}
}

