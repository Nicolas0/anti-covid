package sql;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.*;

import beans.User;
import utils.BCrypt;

public class ConnectionBDD {
//	public ConnectionBDD() {};
	   
	public  ResultSet doRequest(String sql_string) {
	   ResultSet results = null;
	   Connection con = connect();
	   try {
		   Statement stmt = con.createStatement();
		   results = stmt.executeQuery(sql_string);
		} catch (SQLException e) {
		   e.printStackTrace();
		}
	   
	   return results;
	}
	
	public  ResultSet doRequestWithParam(String sql_string,String[] tabStringParam) {
		   ResultSet results = null;
		   Connection con = connect();
//		   System.out.println(sql_string);
			for(String s: tabStringParam) {
				System.out.println(s);
			}
			 
		   try {
			   PreparedStatement stmt = con.prepareStatement(sql_string);
			   for(int i = 0; i < tabStringParam.length; i++) {
				   System.out.println(i + ": " + tabStringParam[i]);
				   stmt.setString(i,tabStringParam[i]);   
				   System.out.println(tabStringParam[i]);

			   }
			   results = stmt.executeQuery(sql_string);
			
		   	} catch (SQLException e) {
			   e.printStackTrace();
			}
		   
		   return results;
		}

	public User getUser(String login, String password) {
		
		   User user = null;
		
		   String rqString1 = "Select * from user where login='"+login+"';";
		   ResultSet res1 = doRequest(rqString1);
		  
		   String dbHashedPwd = "";
		   try {
			   while(res1.next()) {				   
				   dbHashedPwd = res1.getString("hash");
			   }
		   } catch (SQLException e) {
			e.printStackTrace();
		   }
		   
		   System.out.println("dbHashedPwd: " + dbHashedPwd);
		   System.out.println("password: " + password);

		   System.out.println(BCrypt.checkpw(password, dbHashedPwd));

		   if(BCrypt.checkpw(password, dbHashedPwd)) {
			   String rqString2 = "Select * from user where login='"+login+"';";
			   ResultSet res2 = doRequest(rqString2);
			  
			   user = new User();
			   try {
				   while(res2.next()) {
					user.setLogin(res2.getString("login"));
					user.setHash(res2.getString("hash"));
					user.setNom(res2.getString("nom"));
					user.setPrenom(res2.getString("prenom"));
					user.setRang(res2.getString("rang"));
				   }
			   } catch (SQLException e) {
					e.printStackTrace();
				}
			}
		   
		   return user;
	   }
	
		public void createUser(String login, String password, String nom, String prenom, String mail) {
			
		   Connection con = connect();
		   
		   String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt(12));
		   System.out.println(hashedPassword);
//		   boolean matched = BCrypt.checkpw(password, generatedSecuredPasswordHash);
//		   System.out.println(matched);

		   try {
		    	Statement stmt = con.createStatement();
		    	String rqString = "INSERT INTO user (login,prenom,nom,mail,rang,hash) VALUES ('"+
						   login+"','"+prenom+"','"+nom+"','"+mail+"', 'basic_user', '" + hashedPassword + "')";
				stmt.executeUpdate(rqString);
			} 
		    catch (SQLException e) {
				e.printStackTrace();
			}
	   }
	
	public Connection connect() {
		   
	 Connection con = null;
	   
	   try {
		   Class.forName("com.mysql.cj.jdbc.Driver");
	   }
	   catch (ClassNotFoundException e) {
		   System.out.println("Impossible de charger le pilote jdbc");
	   }

	   System.out.println("connexion a la base de données");
	   
	   try {
	         String DBurl = "jdbc:mysql://localhost:3306/projetWeb";
	         con = DriverManager.getConnection(DBurl,"cours_univ","");
	         System.out.println("connexion réussie");
	   } 
	   catch (SQLException e) {
		   System.out.println("Connection à la base de données impossible");
	   }
	   
	   return con;
   }
	
	private static void arret(String message) {
		System.err.println(message);
		System.exit(99);
	}
}
